import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard,3, this.playerNumber);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);


            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
        double opp3s = 0, my3s = 0, opp2s = 0, my2s = 0,myMid = 0, oppMid = 0,sum;
        int oppNumber;
        int [][] board =gameBoard.getBoard();
        if (playerNumber == 1) {
            oppNumber = 2;
        }else{
            oppNumber = 1;
        }
        for (int j = 0; j < 7; j++){
            if (board[j][3] == oppNumber){
                oppMid++;
            }
            else if (board[j][3] == playerNumber){
                myMid++;
            }
        }
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (board[i][j] == oppNumber) {
                    if(i<6&&j<6) {
                        if (board[i + 1][j + 1] == oppNumber) {
                            opp2s++;
                            if (j < 5 && i < 5) {
                                if (board[i + 2][j + 2] == oppNumber) {
                                    opp3s++;
                                }
                            }
                        }
                    }
                    if(j<6) {
                        if (board[i][j + 1] == oppNumber) {
                            opp2s++;
                            if (j < 5) {
                                if (board[i][j + 2] == oppNumber) {
                                    opp3s++;
                                }
                            }
                        }
                    }
                    if(i<6) {
                        if (board[i + 1][j] == oppNumber) {
                            opp2s++;
                            if (i < 5) {
                                if (board[i + 2][j] == oppNumber) {
                                    opp3s++;
                                }
                            }
                        }
                    }
                    if (i>0){
                        if(j<6) {
                            if (board[i - 1][j + 1] == oppNumber) {
                                opp2s++;
                                if (j < 5 && i > 1) {
                                    if (board[i - 2][j + 2] == oppNumber) {
                                        opp3s++;
                                    }
                                }
                            }
                        }
                        if(j>0) {
                            if (board[i - 1][j - 1] == oppNumber) {
                                opp2s++;
                                if (i>1&&j>1) {
                                    if (board[i - 2][j - 2] == oppNumber) {
                                        opp3s++;
                                    }
                                }
                            }
                        }
                        if (board[i - 1][j] == oppNumber) {
                            opp2s++;
                            if(i > 1) {
                                if (board[i - 2][j] == oppNumber) {
                                    opp3s++;
                                }
                            }
                        }
                    }
                }
                if (board[i][j] == playerNumber) {
                    if(i<6&&j<6) {
                        if (board[i + 1][j + 1] == playerNumber) {
                            my2s++;
                            if (j < 5 && i < 5) {
                                if (board[i + 2][j + 2] == playerNumber) {
                                    my3s++;
                                }
                            }
                        }
                    }
                    if(j<6) {
                        if (board[i][j + 1] == playerNumber) {
                            my2s++;
                            if (j < 5) {
                                if (board[i][j + 2] == playerNumber) {
                                    my3s++;
                                }
                            }
                        }
                    }
                    if(i<6) {
                        if (board[i + 1][j] == playerNumber) {
                            my2s++;
                            if (i < 5) {
                                if (board[i + 2][j] == playerNumber) {
                                    my3s++;
                                }
                            }
                        }
                    }

                    if (i>0){
                        if(j<6) {
                            if (board[i - 1][j + 1] == playerNumber) {
                                my2s++;
                                if (j < 5 && i > 1) {
                                    if (board[i - 2][j + 2] == playerNumber) {
                                        my3s++;
                                    }
                                }
                            }
                        }
                        if(j>0) {
                            if (board[i - 1][j - 1] == playerNumber) {
                                my2s++;
                                if (i>1&&j>1) {
                                    if (board[i - 2][j - 2] == playerNumber) {
                                        my3s++;
                                    }
                                }
                            }
                        }
                        if (board[i - 1][j] == playerNumber) {
                            my2s++;
                            if(i > 1) {
                                if (board[i - 2][j] == playerNumber) {
                                    my3s++;
                                }
                            }
                        }
                    }
                }
            }
        }
        sum = (my2s*2)+(my3s*3)+(opp2s*2)+(opp3s*5)+(oppMid*1)+(myMid*2.1);
        if (sum == 0){
            return 0.0;
        }
        else{
            return (((my2s*2+my3s*3+myMid*2.1)-(opp2s*2+opp3s*5+oppMid*1))/sum);
        }
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0

    }


    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }



}
